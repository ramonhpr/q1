import socket
from threading import *
import time
import os

#--------------------------------------VARIAVEIS GLOBAIS------------------------------------------------------

x=[[0,0,0],[0,0,0],[0,0,0]]                         #matriz do jogo da velha
vitoriasx=0
vitoriaso=0
playAgain='Y'                                       #variavel global que diz se o jogador quer jogar de novo
acc='n'                                             #variavel q diz se o oponente aceitou o pedido de jogo

#--------------------------------------LOGICA DO JOGO---------------------------------------------------------

def codifica_jogada(x,player):  #transforma o q o kara apertou no teclado na matriz x
    data=None
    while True:
        data = raw_input("Digite sua jogada:")
        y=0
        print 'player=',player
        if data == 'q' and x[0][0]==0:
            y=1
            x[0][0]=player
            print x
            break
        elif data == 'w'and x[0][1]==0:
            y=1
            x[0][1]=player
            break
        elif data == 'e'and x[0][2]==0:
            y=1
            x[0][2]=player
            break
        elif data == 'a'and x[1][0]==0:
            y=1
            x[1][0]=player
            break
        elif data == 's'and x[1][1]==0:
            y=1
            x[1][1]=player
            break
        elif data == 'd'and x[1][2]==0:
            y=1
            x[1][2]=player
            break
        elif data == 'z'and x[2][0]==0:
            y=1
            x[2][0]=player
            break
        elif data == 'x'and x[2][1]==0:
            y=1
            x[2][1]=player
            break
        elif data == 'c'and x[2][2]==0:
            y=1
            x[2][2]=player
            break
        if y == 0:
            print 'Comando invalido! Digite de novo'
    
    return x

def print_velha(valor):                     #limpa a tela e desenha o placar e o jogo da velha apartir da matriz
    global vitoriasx,vitoriaso
    os.system('clear')
    print '         x    o'
    print 'Placar: ',vitoriasx,'vs',vitoriaso
    for i in range(0,3):
        for j in range(0,3):
            if valor[i][j]==0:
                print " ",
            elif valor[i][j] == 1:
                print "x",
            elif valor[i][j] == 2:
                print "o",
            if j!=2:
                print "|",
        if i != 2:
            print "\n---------"
    print '\n'

def verifica_ganho(x): #retorna uma lista de dois elementos onde o primeiro eh um Bool que diz se alguem ganhou,o outro diz quem ganhou x ou o
    ret = False
    vencedor=0
    for i in range(0,3):
        if x[i]==[1,1,1] or x[i]==[2,2,2]:
            ret = True
            vencedor=x[i][0]
        if x[0][0]==x[1][1]==x[2][2]!=0 :
            ret = True
            vencedor=x[0][0]
        if x[0][2]==x[1][1]==x[2][0]!=0:
            ret = True
            vencedor=x[0][2]
        if x[0][0]==x[1][0]==x[2][0]!=0:
            ret=True
            vencedor=x[0][0]
        if x[0][1]==x[1][1]==x[2][1]!=0:
            ret = True
            vencedor=x[0][1]
        if x[0][2]==x[1][2]==x[2][2]!=0:
            ret=True
            vencedor=x[0][2]

    return [ret,vencedor]  


def verifica_velha(x):          #verifica se deu empate
    ret=True
    if verifica_ganho(x)[0]:
        ret = False
    else:
        for i in range(0,3):
            for j in range(0,3):
                if x[i][j]!=0 and ret:
                    ret = True
                else:
                    ret=False

    return ret

def codifica_matriz(x):                 #codificando a matriz como uma string para ser enviada para ao oponente
    lista = []
    string=''
    for i in range(0,3):
        for j in range(0,3):
            lista.append(str(x[i][j]))
    return string.join(lista)

def decodifica_matriz(s):                       #decodifica a string recebida do oponente e converte numa matriz
    matriz=[range(0,3),range(0,3),range(0,3)]   #inicializa essas variaves com valores quaisquer
    for i in range(0,3):
        for j in range(0,3):
            if s[3*i+j]=='0':
                matriz[i][j]=0
            elif s[3*i+j]=='1':
                matriz[i][j]=1
            else:
                matriz[i][j]=2
    #print "matriz decodificada:",matriz
    return matriz


#--------------------------------------COMUNICACAO ENTRE HOSTS E DESENHO----------------------------- 


'''
Resumo de como a variavel lock funciona:
com a declaracao:
"
with lock:  #a thread atual adquire um lock antes de executar a primeira instrucao do escopo do with
    ....instrucoes quaisquer
    lock.wait()     #espera ate outra thread utilizar a funcao lock.notify() [OBS: so pode dar wait se estiver dentro de um with lock]
#aqui a thread liberou o lock 
"
lock.notify() = notifica a thread que esta no esperando, q ja eh hora de parar de esperar

'''

def envia_dados(servHOST, servPORT,lock,player): 
    global x,vitoriasx,vitoriaso,playAgain,acc
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    dest = (servHOST, servPORT)
    bytesEnviados=0
    check=True
    cont=0
    while check:
        try:
            if player==1:
                acc='y'
                with lock:          #tem q ver essa parte aki dps
                    lock.notify()
            else:
                acc=raw_input('Vc aceita jogar com %s?(Y/N)' %servPORT)
            if acc.upper() == 'Y':
                tcp.connect((servHOST,servPORT))
                playAgain='y'
                check=False
            elif acc.upper() == 'N':
                playAgain='n'
                check=False
            else:
                print 'So pode colocar y ou n'
        except:
            if cont==0 and player == 1:
                print 'Esperando aceitar...'
                cont+=1
            playAgain='n'
    while playAgain.upper()=='Y':                                       #se nao quiser jogar de novo sai do jogo
        if player ==1:
            while not verifica_ganho(x)[0]:                     #enquanto n tiver ganhador continue no loop
                print_velha(x)
                print 'Vez do jogador x:'
                #time.sleep(1)
                if verifica_ganho(x)[1]==1:
                    vitoriasx+=1
                    print_velha(x)
                    print "Jogador x ganhou!"
                    time.sleep(1)
                    lock.notify()           
                    break
                elif verifica_ganho(x)[1]==2:
                    vitoriaso+=1
                    print_velha(x)
                    print "Jogador o ganhou!"
                    time.sleep(1)
                    lock.notify()
                    break

                if verifica_velha(x):
                    print "Deu velha!\nQuer jogar de novo(y/n)?"            #Tem q fazer a pergunta aki por causa do clear
                    time.sleep(1)
                    break
                x=codifica_jogada(x,player)     
                msg=codifica_matriz(x)
                #time.sleep(1)
                try:
                    bytesEnviados = tcp.send(msg)
                except:
                    print 'Oponente saiu da partida! O programa sera fechado'
                    time.sleep(1)
                    os._exit(1)
                    break
                print_velha(x)
                #print 'bloqueando a thread de enviar'
                print 'Vez do jogador o ...'
                with lock:
                    lock.notify()
                    #lock.notify()
                    #print 'Espera receber...'
                    lock.wait()

        else:   #tem q testar isso aki
            print_velha(x)
            with lock:
                lock.notify()       #notifica a thread q espera pela resposta se aceita o jogo
            while not verifica_ganho(x)[0]:
                print 'Vez do jogador x...'
                with lock:
                    #print 'Thread q envia espera'
                    lock.wait()
                    print_velha(x)
                    #print 'Thread de envio liberada'
                    #time.sleep(2)
                    if verifica_ganho(x)[1]==1:
                        print "Jogador x ganhou!\nQuer jogar de novo(y/n)?"             #Tem q fazer a pergunta aki por causa do clear
                        time.sleep(1)
                        lock.notify()
                        break
                    elif verifica_ganho(x)[1]==2:
                        print "Jogador o ganhou!"
                        time.sleep(1)
                        lock.notify()
                        break
                    else:
                        print 'Vez do jogador o:'
                    if verifica_velha(x):
                        print "Deu velha!\nQuer jogar de novo(y/n)?"                    #Tem q fazer a pergunta aki por causa do clear
                        time.sleep(1)
                        #lock.notify()
                        break
                    x=codifica_jogada(x,player)
                    msg=codifica_matriz(x)
                    #time.sleep(1)
                    bytesEnviados = tcp.send(msg)
                    if verifica_ganho(x)[1]==1:
                        vitoriasx+=1
                        print_velha(x)
                        print "Jogador x ganhou!"
                        time.sleep(1)
                        lock.notify()
                        break
                    elif verifica_ganho(x)[1]==2:
                        vitoriaso+=1
                        print_velha(x)
                        print "Jogador o ganhou!"
                        time.sleep(1)
                        lock.notify()
                        break

                    if verifica_velha(x):
                        print "Deu velha!"
                        time.sleep(1)
                        break
                    print_velha(x)
                    #print 'Agora notifica a thread'
                    lock.notifyAll()
        with lock:
            lock.wait()         #espera a resposta da outra thread para saber se vai jogar de novo

    with lock:
        lock.notify()
    
    tcp.close()
    return bytesEnviados



def recebe_dados(cliHOST, cliPORT,lock,player):
    global x,vitoriasx,vitoriaso,playAgain,acc
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    orig = (cliHOST, cliPORT)
    tcp.bind(orig)  #Criando servidor do peer
    tcp.listen(10)
    connectionSocket, addr = (0,0)
    with lock:                                                  #espera pra ver se aceita ou nao
        lock.wait()

    if acc.upper()=='Y':
        connectionSocket,addr=tcp.accept()
    else:
        print 'Fechando...'

    while playAgain.upper()=='Y':                                       #se nao quiser jogar de novo sai do jogo
        if player ==1:
            while not verifica_ganho(x)[0]:                     #enquanto nao tiver ganhador continue no loop
                #print 'bloqueando a thread de receber'
                with lock:
                    lock.wait()                                 #espera enviar o dado
                    #print 'Thread de recebimento liberada'
                    #time.sleep(1)
                    if verifica_ganho(x)[1]==1:
                        vitoriasx+=1
                        print_velha(x)
                        print "Jogador x ganhou!"
                        time.sleep(1)
                        lock.notify()
                        break
                    elif verifica_ganho(x)[1]==2:
                        print "Jogador o ganhou!"
                        vitoriaso+=1
                        time.sleep(1)
                        lock.notify()
                        break
                    if verifica_velha(x):
                        print "Deu velha!"
                        time.sleep(1)
                        lock.notify()
                        break
                    try:
                        msg= connectionSocket.recv(1024)
                        x=decodifica_matriz(msg)
                    except:
                        print 'Oponente saiu da partida! O programa sera fechado'
                        time.sleep(1)
                        os._exit(1)
                        break
                    if verifica_ganho(x)[1]==1:
                        vitoriasx+=1
                        print_velha(x)
                        print "Jogador x ganhou!"
                        time.sleep(1)
                        lock.notify()
                        break
                    elif verifica_ganho(x)[1]==2:
                        print "Jogador o ganhou!"
                        vitoriaso+=1
                        time.sleep(1)
                        lock.notify()
                        break
                    if verifica_velha(x):
                        print "Deu velha!"
                        time.sleep(1)
                        lock.notify()
                        break
                    lock.notify()

                #print cliente, msg
        else:
            while not verifica_ganho(x)[0] and acc.upper()=='Y':
                #print 'Thread de recebimento liberada'
                #time.sleep(1)
                try:
                    msg = connectionSocket.recv(1024)
                    x=decodifica_matriz(msg)
                except:
                    time.sleep(1)
                    print 'Oponente saiu da partida! Programa sera fechado'
                    os._exit(1)
                    break
                #print cliente, msg
                #print 'bloqueando a thread de receber'
                with lock:
                    if verifica_ganho(x)[1]==1:
                        vitoriasx+=1
                        print_velha(x)
                        print "Jogador x ganhou!"
                        time.sleep(1)
                        lock.notify()
                        break
                    elif verifica_ganho(x)[1]==2:
                        vitoriaso+=1
                        print_velha(x)
                        print "Jogador o ganhou!"
                        time.sleep(1)
                        lock.notify()
                        break
                    if verifica_velha(x):
                        print "Deu velha!"
                        time.sleep(1)
                        lock.notify()
                        break      
                    lock.notify()
                    lock.wait()
        while acc.upper()=='Y':
            playAgain=raw_input('Quer jogar de novo(y/n)?')      #verifica se quer jogar de novo
            if playAgain.upper()=='Y':                          #se sim zera a matriz
                x=[[0,0,0],[0,0,0],[0,0,0]]
                with lock:
                    lock.notify()                               #notifica a outra thread agora
                break
            elif playAgain.upper()=='N':                        #senao pare a verificacao de erro
                print 'Fechando...'
                connectionSocket.close()
                with lock:
                    lock.notify()                               #notifica a outra thread agora
                break
            else:
                print 'Erro! Digite de novo ou y ou n'

    tcp.close()




#--------------------------------------------MAIN----------------------------------------

print 'QUESTAO 1- IF672 \n\nJOGO DA VELHA'

#cliHOST = raw_input('Meu IP: ') 
cliHOST = socket.gethostbyname(socket.gethostname())

print 'SEU IP EH:',cliHOST
cliPORT = input('Minha Porta: ')


servHOST = raw_input('IP do oponente: ')               # Endereco IP do Servidor
if servHOST =='':
    servHOST=cliHOST
servPORT = input('Porta do oponente: ')            # Porta que o Servidor esta


#cria uma variavel de condicao/bloqueio para as threads
varLock=Condition()

#Tem que definir quem eh o player de alguma formar


player=None
if cliPORT > servPORT:
    player=1
    print 'vc eh  x'
else:
    player=2
    print 'vc eh 0'

'''
cria um objeto thread para
o cliente, q vai rodar a 
funcao que envia dados

#se sua porta for impar vc eh x 
(pessimo jeito por sinal o usuario teria
q saber q teria q colocar a outra portar impar,
senao teria dois x ou dois o)
'''
tClient = Thread(target=envia_dados,args=(servHOST, servPORT,varLock,player),name="Thread Client")

'''
cria um objeto 
thread para o servidor, 
q vai rodar a funcao que recebe dados
'''
tServer = Thread(target=recebe_dados,args=(cliHOST, cliPORT,varLock,player),name="Thread Server")

#roda a thread cliente
tClient.start()
#roda a thread servidor
tServer.start()
